package app.parqueo.application;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import app.parqueo.domain.persistence_ports.TransaccionPersistence;

@Service
public class TransaccionService {
	TransaccionPersistence transaccionPersistence;
	
	public TransaccionService(TransaccionPersistence transaccionPersistence)
	{
		this.transaccionPersistence = transaccionPersistence;
	}
	
	public Integer recargarBilletera(Integer clie_id, BigDecimal monto, Integer frec_id, Integer tarj_id) {
		return this.transaccionPersistence.recargarBilletera(clie_id,monto,frec_id,tarj_id);
	}
	
	public Integer validarRecargaBilletera(Integer clie_id, Integer frec_id, Integer tarj_id) {
		return this.transaccionPersistence.validarRecargaBilletera(clie_id, frec_id, tarj_id);
	}
	

	
}
