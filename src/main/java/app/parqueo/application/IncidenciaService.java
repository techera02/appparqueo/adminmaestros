package app.parqueo.application;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ResponseIncidencia;
import app.parqueo.domain.model.ResponseIncidencia2;
import app.parqueo.domain.persistence_ports.IncidenciaPersistence;

@Service
public class  IncidenciaService {

	
	IncidenciaPersistence incidenciaPersistence;
	
	public IncidenciaService(IncidenciaPersistence incidenciaPersistence)
	{
		this.incidenciaPersistence = incidenciaPersistence;
	}
	
	public List<ResponseIncidencia> listarIncidencia(Integer pagenumber, Integer pagesize,
			Date fechainicio, Integer indiceFechainicio,
			Date fechafin, Integer indiceFechafin,
			Integer tipoincidencia, String placa)
	{
        return this.incidenciaPersistence.listarIncidencia(pagenumber,pagesize,fechainicio,indiceFechainicio,
        		fechafin,indiceFechafin,tipoincidencia,placa);
    }
	
	public Integer validarIncidenciaExiste(Integer idincidencia) {
		return this.incidenciaPersistence.validarIncidenciaExiste(idincidencia);
	}
	
	public ResponseIncidencia2 seleccionarIncidenciaFoto(Integer idincidencia) {
		return this.incidenciaPersistence.seleccionarIncidenciaFoto(idincidencia);
	}
}
