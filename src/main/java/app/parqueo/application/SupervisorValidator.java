package app.parqueo.application;

import app.parqueo.domain.model.ResponseInsertarSupervisor;
import app.parqueo.domain.model.Supervisor;


public class SupervisorValidator {

	public ResponseInsertarSupervisor InsertarSupervisor(Supervisor supervisor)
	{
		
		ResponseInsertarSupervisor response = new ResponseInsertarSupervisor();
		
		response.setCodError(null);
		response.setEstado(true);
		response.setMensaje(null);	
		
		if(supervisor.getSupe_TokenFirebase() == null  || supervisor.getSupe_TokenFirebase() == "") {
			response.setCodError(1);
			response.setEstado(false);
			response.setMensaje("Token vacío o nulo");
		}

		return response;
	}
	
	
}
