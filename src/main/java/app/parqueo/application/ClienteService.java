package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ResponseCliente;
import app.parqueo.domain.model.ResponseDetalleCliente;
import app.parqueo.domain.persistence_ports.ClientePersistence;

@Service
public class ClienteService {
	
	ClientePersistence clientePersistence;
	
	public ClienteService(ClientePersistence clientePersistence)
	{
		this.clientePersistence = clientePersistence;
	}

	public List<ResponseCliente> listarCliente(Integer pagenumber, Integer pagesize,String nombre,
			String nrodocumento,String placa) 
	{
        return this.clientePersistence.listarCliente(pagenumber,pagesize,nombre,nrodocumento,placa);
    }
	
	public Integer validarClienteExiste(Integer idcliente) {
		return this.clientePersistence.validarClienteExiste(idcliente);
	}
	
	public ResponseDetalleCliente seleccionarDetalleCliente(Integer idcliente) {
		return this.clientePersistence.seleccionarDetalleCliente(idcliente);
	}
}