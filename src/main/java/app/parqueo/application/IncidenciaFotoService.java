package app.parqueo.application;

import java.util.List;

import app.parqueo.domain.model.IncidenciaFoto;
import app.parqueo.domain.persistence_ports.IncidenciaFotoPersistence;

public class IncidenciaFotoService {

	IncidenciaFotoPersistence incidenciaFotoPersistence;
	
	public IncidenciaFotoService(IncidenciaFotoPersistence incidenciaFotoPersistence)
	{
		this.incidenciaFotoPersistence = incidenciaFotoPersistence;
	}
	
	public List<IncidenciaFoto> listarIncidenciaFoto(Integer idincidencia)
	{
        return this.incidenciaFotoPersistence.listarIncidenciaFoto(idincidencia);
    }
}
