package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.ResponseSupervisor;
import app.parqueo.domain.model.Supervisor;
import app.parqueo.domain.persistence_ports.SupervisorPersistence;

@Service
public class  SupervisorService {
	
	SupervisorPersistence supervisorPersistence;
	
	public SupervisorService(SupervisorPersistence supervisorPersistence)
	{
		this.supervisorPersistence = supervisorPersistence;
	}
	public Integer insertarSupervisor(Supervisor supervisor) 
	{
        return this.supervisorPersistence.insertarSupervisor(supervisor);
    }
	
	public Integer actualizarSupervisor(Supervisor supervisor) {
		return this.supervisorPersistence.actualizarSupervisor(supervisor);
	}
	
	public Integer verificarCorreo(String correo, Integer idSupervisor) 
	{
        return this.supervisorPersistence.verificarCorreo(correo, idSupervisor);
    }
	
	public List<ResponseSupervisor> listarSupervisor(Integer pagenumber, Integer pagesize, String nombre,
			String documento, Integer activo)
	{
        return this.supervisorPersistence.listarSupervisor(pagenumber,pagesize,nombre,documento,activo);
    }
	
	public Integer validarAliasSupervisor(String supe_alias) {
        return this.supervisorPersistence.validarAliasSupervisor(supe_alias);
	}
	
	public Integer validarSupervisorExiste(Integer idsupervisor) {
		return this.supervisorPersistence.validarSupervisorExiste(idsupervisor);
	}
	
	public Integer eliminarSupervisor(Integer idsupervisor) {
		return this.supervisorPersistence.eliminarSupervisor(idsupervisor);
	}
	
	public Integer validarDocumento(String documento, Integer idSupervisor) 
	{
        return this.supervisorPersistence.validarDocumento(documento, idSupervisor);
    }
	
	public Supervisor seleccionarSupervisor(Integer idsupervisor) {
		return this.supervisorPersistence.seleccionarSupervisor(idsupervisor);
	}
	
	public Integer recuperarContraseniaSupervisor(String correo, String clave) {
		return this.supervisorPersistence.recuperarContraseniaSupervisor(correo, clave);
	}
	
	public Supervisor seleccionarSupervisorPorCorreo(String correo) {
		return this.supervisorPersistence.seleccionarSupervisorPorCorreo(correo);
	}
}
