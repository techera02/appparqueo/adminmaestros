package app.parqueo.application;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

@Service
public class EmailService {

public static void enviarMail(String destinatario, String asunto, String plantilla, Context variables) {		
		
		String host = "mail.techeraperu.com";
		String user = "webmaster@techeraperu.com";
		String clave = "techera3366";
		String auth = "true";
		String port = "587";
		
		
	    Properties props = System.getProperties();
	    props.put("mail.smtp.host", host);
	    props.put("mail.smtp.user", user);
	    props.put("mail.smtp.clave", clave);
	    props.put("mail.smtp.auth", auth);
	    props.put("mail.smtp.starttls.enable", "true");
	    props.put("mail.smtp.port", port);

	    Session session = Session.getDefaultInstance(props);
	    MimeMessage message = new MimeMessage(session);

	    try {
	    	
	    	ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		    templateResolver.setPrefix("templates/");
		    templateResolver.setSuffix(".html");
		    templateResolver.setTemplateMode(TemplateMode.HTML);
		    templateResolver.setCharacterEncoding("UTF-8");
		    templateResolver.setOrder(1);
		    templateResolver.setCheckExistence(true);
			
			TemplateEngine templateEngine = new TemplateEngine();
		    templateEngine.setTemplateResolver(templateResolver);
			
			String htmlContent = templateEngine.process(plantilla, variables);
	    	
	        message.setFrom(new InternetAddress(user));
	        message.addRecipients(Message.RecipientType.TO, destinatario);
	        message.setSubject(asunto);
	        message.setText(htmlContent, "utf-8", "html");
	        Transport transport = session.getTransport("smtp");
	        transport.connect(host, user, clave);
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	    }
	    catch (MessagingException me) {
	        me.printStackTrace();
	    }
	}
}
