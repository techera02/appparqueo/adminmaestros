package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.TipoDocumento;
import app.parqueo.domain.persistence_ports.TipoDocumentoPersistence;

@Service
public class TipoDocumentoService {

	TipoDocumentoPersistence tipoDocumentoPersistence;
	
	public TipoDocumentoService(TipoDocumentoPersistence tipoDocumentPersistence)
	{
		this.tipoDocumentoPersistence = tipoDocumentPersistence;
	}
	
	public List<TipoDocumento> listarTipoDocumento()
	{
        return this.tipoDocumentoPersistence.listarTipoDocumento();
    }
}
