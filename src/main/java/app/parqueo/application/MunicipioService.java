package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Municipio;
import app.parqueo.domain.model.ResponseMunicipio;
import app.parqueo.domain.persistence_ports.MunicipioPersistence;

@Service
public class  MunicipioService {
	
	MunicipioPersistence municipioPersistence;
	
	public MunicipioService(MunicipioPersistence municipioPersistence)
	{
		this.municipioPersistence = municipioPersistence;
	}

	public List<ResponseMunicipio> listarMunicipio(Integer pagenumber, Integer pagesize, String nombre, Integer activo) 
	{
        return this.municipioPersistence.listarMunicipio(pagenumber,pagesize,nombre,activo);
    }
	
	public Integer insertarMunicipio(Municipio municipio) 
	{
        return this.municipioPersistence.insertarMunicipio(municipio);
    }
	
	public Integer actualizarMunicipio(Municipio municipio) {
		return this.municipioPersistence.actualizarMunicipio(municipio);
	}
	
	public Integer verificarNombre(String muni_nombre, Integer idMunicipio) 
	{
        return this.municipioPersistence.verificarNombre(muni_nombre, idMunicipio);
    }
	
	public Integer validarMunicipioExiste(Integer idmunicipio) {
		return this.municipioPersistence.validarMunicipioExiste(idmunicipio);
	}
	
	public Municipio seleccionarMunicipio(Integer idmunicipio) {
		return this.municipioPersistence.seleccionarMunicipio(idmunicipio);
	}
	
	public Integer eliminarMunicipio(Integer idmunicipio, Integer idtarifa) {
		return this.municipioPersistence.eliminarMunicipio(idmunicipio, idtarifa);
	}
	
	public Integer validarMunicipioDependencias(Integer idmunicipio) {
		return this.municipioPersistence.validarMunicipioDependencias(idmunicipio);
	}
}

