package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.Municipio;
import app.parqueo.domain.model.ResponseMunicipio;

public interface MunicipioPersistence {
	
	List<ResponseMunicipio> listarMunicipio(Integer pagenumber, Integer pagesize, String nombre, Integer activo);
	Integer insertarMunicipio(Municipio municipio);
	Integer actualizarMunicipio(Municipio municipio);
	Integer verificarNombre(String muni_nombre, Integer idMunicipio);
	Integer validarMunicipioExiste(Integer idmunicipio);
	Integer validarMunicipioDependencias(Integer idmunicipio);
	Municipio seleccionarMunicipio(Integer idmunicipio);	
	Integer eliminarMunicipio(Integer idmunicipio, Integer idtarifa);
}
