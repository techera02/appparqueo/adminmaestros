package app.parqueo.domain.persistence_ports;

import java.math.BigDecimal;

public interface TransaccionPersistence {
	
	Integer recargarBilletera(Integer clie_id, BigDecimal monto, Integer frec_id, Integer tarj_id);
	
	Integer validarRecargaBilletera(Integer clie_id, Integer frec_id, Integer tarj_id);
	

}
