package app.parqueo.domain.persistence_ports;

import java.sql.Date;
import java.util.List;

import app.parqueo.domain.model.ResponseIncidencia;
import app.parqueo.domain.model.ResponseIncidencia2;

public interface IncidenciaPersistence 
{
	List<ResponseIncidencia> listarIncidencia(Integer pagenumber, Integer pagesize, 
			Date fechainicio, Integer indiceFechainicio,
			Date fechafin, Integer indiceFecafin,
			Integer tipoincidencia, String placa);
	
	Integer validarIncidenciaExiste(Integer idincidencia);
	
	ResponseIncidencia2 seleccionarIncidenciaFoto(Integer idincidencia);
}
