package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.ResponseSupervisor;
import app.parqueo.domain.model.Supervisor;

public interface SupervisorPersistence {
	
	Integer insertarSupervisor(Supervisor supervisor);
	Integer actualizarSupervisor(Supervisor supervisor);
	Integer verificarCorreo(String correo, Integer idSupervisor);
	Integer validarAliasSupervisor(String supe_alias);
	List<ResponseSupervisor> listarSupervisor(Integer pagenumber, Integer pagesize, String nombre, String documento, Integer activo);
	Integer validarSupervisorExiste(Integer idsupervisor);
	Integer eliminarSupervisor(Integer idsupervisor);
	Integer validarDocumento(String documento, Integer idSupervisor);
	Supervisor seleccionarSupervisor(Integer idsupervisor);
	Integer recuperarContraseniaSupervisor(String correo, String clave);
	Supervisor seleccionarSupervisorPorCorreo(String correo);
}
