package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.IncidenciaFoto;

public interface IncidenciaFotoPersistence {

	List<IncidenciaFoto> listarIncidenciaFoto(Integer idincidencia);
}
