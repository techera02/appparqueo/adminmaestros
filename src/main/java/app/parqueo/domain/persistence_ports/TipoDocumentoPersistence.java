package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.TipoDocumento;

public interface TipoDocumentoPersistence {

	List<TipoDocumento> listarTipoDocumento();
	
}