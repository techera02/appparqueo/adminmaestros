package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.ResponseCliente;
import app.parqueo.domain.model.ResponseDetalleCliente;

public interface ClientePersistence {
	
	List<ResponseCliente> listarCliente(Integer pagenumber, Integer pagesize,String nombre,
			String nrodocumento,String placa);

	Integer validarClienteExiste(Integer idcliente);
	
	ResponseDetalleCliente seleccionarDetalleCliente(Integer idcliente);
}