package app.parqueo.domain.model;

public class ResponseSeleccionarMunicipio extends ResponseError{

	private Municipio municipio;

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
