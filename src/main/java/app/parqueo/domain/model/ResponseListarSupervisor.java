package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarSupervisor extends ResponseError{
	
	private List<ResponseSupervisor> lista;

	public List<ResponseSupervisor> getLista() {
		return lista;
	}

	public void setLista(List<ResponseSupervisor> lista) {
		this.lista = lista;
	}
}



