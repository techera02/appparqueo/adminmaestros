package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarTipoDocumento extends ResponseError{

	private List<TipoDocumento> lista;

	public List<TipoDocumento> getLista() {
		return lista;
	}

	public void setLista(List<TipoDocumento> lista) {
		this.lista = lista;
	}
}
