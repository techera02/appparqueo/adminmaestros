package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarIncidencia extends ResponseError{

	private List<ResponseIncidencia> lista;

	public List<ResponseIncidencia> getLista() {
		return lista;
	}

	public void setLista(List<ResponseIncidencia> lista) {
		this.lista = lista;
	}
}
