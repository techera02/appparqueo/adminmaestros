package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarCliente extends ResponseError{
	
	private List<ResponseCliente> lista;

	public List<ResponseCliente> getLista() {
		return lista;
	}

	public void setLista(List<ResponseCliente> lista) {
		this.lista = lista;
	}
}


