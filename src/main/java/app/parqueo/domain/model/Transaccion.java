package app.parqueo.domain.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

public class Transaccion {
	private BigInteger tran_Id;
	private Integer clie_Id;
	private char tran_Tipo;
	private BigDecimal tran_Monto;
	private BigInteger parq_Id;
	private Integer frec_Id;
	private Integer tarj_Id;
	private Timestamp tran_FechaCreacion;
	private Integer tran_UsuarioCreacion;
	private BigDecimal tran_SaldoActual;	

	
	public BigInteger getTran_Id() {
		return tran_Id;
	}
	public void setTran_Id(BigInteger tran_Id) {
		this.tran_Id = tran_Id;
	}
	public Integer getClie_Id() {
		return clie_Id;
	}
	public void setClie_Id(Integer clie_Id) {
		this.clie_Id = clie_Id;
	}
	public char getTran_Tipo() {
		return tran_Tipo;
	}
	public void setTran_Tipo(char tran_Tipo) {
		this.tran_Tipo = tran_Tipo;
	}
	public BigDecimal getTran_Monto() {
		return tran_Monto;
	}
	public void setTran_Monto(BigDecimal tran_Monto) {
		this.tran_Monto = tran_Monto;
	}
	public BigInteger getParq_Id() {
		return parq_Id;
	}
	public void setParq_Id(BigInteger parq_Id) {
		this.parq_Id = parq_Id;
	}
	public Integer getFrec_Id() {
		return frec_Id;
	}
	public void setFrec_Id(Integer frec_Id) {
		this.frec_Id = frec_Id;
	}
	public Integer getTarj_Id() {
		return tarj_Id;
	}
	public void setTarj_Id(Integer tarj_Id) {
		this.tarj_Id = tarj_Id;
	}
	public Timestamp getTran_FechaCreacion() {
		return tran_FechaCreacion;
	}
	public void setTran_FechaCreacion(Timestamp tran_FechaCreacion) {
		this.tran_FechaCreacion = tran_FechaCreacion;
	}
	public Integer getTran_UsuarioCreacion() {
		return tran_UsuarioCreacion;
	}
	public void setTran_UsuarioCreacion(Integer tran_UsuarioCreacion) {
		this.tran_UsuarioCreacion = tran_UsuarioCreacion;
	}
	public BigDecimal getTran_SaldoActual() {
		return this.tran_SaldoActual;
	}
	public void setTran_SaldoActual(BigDecimal tran_SaldoActual) {
		this.tran_SaldoActual = tran_SaldoActual;
	}
	

	
	
}
