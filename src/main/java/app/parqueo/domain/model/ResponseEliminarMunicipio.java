package app.parqueo.domain.model;

public class ResponseEliminarMunicipio extends ResponseError{

	private boolean resultado;

	public boolean isResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
}
