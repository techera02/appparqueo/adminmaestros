package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarMunicipio extends ResponseError{
	
	private List<ResponseMunicipio> lista;

	public List<ResponseMunicipio> getLista() {
		return lista;
	}

	public void setLista(List<ResponseMunicipio> lista) {
		this.lista = lista;
	}
}


