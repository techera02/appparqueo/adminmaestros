package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"IncidenciaFoto\"")
public class IncidenciaFotoEntity {

	@Id
	private Integer Ifot_Id;
	private Integer Inci_Id;
	private String Ifot_NombreArchivo;
	
	public Integer getIfot_Id() {
		return Ifot_Id;
	}
	public void setIfot_Id(Integer ifot_Id) {
		Ifot_Id = ifot_Id;
	}
	public Integer getInci_Id() {
		return Inci_Id;
	}
	public void setInci_Id(Integer inci_Id) {
		Inci_Id = inci_Id;
	}
	public String getIfot_NombreArchivo() {
		return Ifot_NombreArchivo;
	}
	public void setIfot_NombreArchivo(String ifot_NombreArchivo) {
		Ifot_NombreArchivo = ifot_NombreArchivo;
	}
}
