package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Supervisor;

@Entity
@Table(name="\"Supervisor\"")
public class SupervisorEntity {

	@Id
	private Integer Supe_Id;
	private String Supe_Nombres;
	private String Supe_ApellidoPaterno;
	private String Supe_ApellidoMaterno;
	private Integer Tdoc_Id;
	private String Supe_NroDocumento;
	private String Supe_TokenFirebase;
	private Boolean Supe_Activo;
	private Integer Supe_UsuarioCreacion;
	private Integer Supe_UsuarioEdicion;
	private Timestamp Supe_FechaCreacion;
	private Timestamp Supe_FechaEdicion;
	private String Supe_Correo;
	private String Supe_Alias;
	private String Supe_Clave;
	
	public String getSupe_Alias() {
		return Supe_Alias;
	}
	public void setSupe_Alias(String supe_Alias) {
		Supe_Alias = supe_Alias;
	}
	public String getSupe_Clave() {
		return Supe_Clave;
	}
	public void setSupe_Clave(String supe_Clave) {
		Supe_Clave = supe_Clave;
	}
	public String getSupe_Correo() {
		return Supe_Correo;
	}
	public void setSupe_Correo(String supe_Correo) {
		Supe_Correo = supe_Correo;
	}
	public int getSupe_Id() 
	{
		return Supe_Id;
	}
	public void setSupe_Id(int supe_Id) 
	{
		Supe_Id = supe_Id;
	}
	
	public String getSupe_Nombres() 
	{
		return Supe_Nombres;
	}
	public void setSupe_Nombres(String supe_Nombres) 
	{
		Supe_Nombres = supe_Nombres;
	}
	
	public String getSupe_ApellidoPaterno() 
	{
		return Supe_ApellidoPaterno;
	}
	public void setSupe_ApellidoPaterno(String supe_ApellidoPaterno) 
	{
		Supe_ApellidoPaterno = supe_ApellidoPaterno;
	}
	
	public String getSupe_ApellidoMaterno() 
	{
		return Supe_ApellidoMaterno;
	}
	public void setSupe_ApellidoMaterno(String supe_ApellidoMaterno) 
	{
		Supe_ApellidoMaterno = supe_ApellidoMaterno;
	}
	
	public int getTdoc_Id() 
	{
		return Tdoc_Id;
	}
	public void setTdoc_Id(int tdoc_Id) 
	{
		Tdoc_Id = tdoc_Id;
	}
	
	public String getSupe_NroDocumento() 
	{
		return Supe_NroDocumento;
	}
	public void setSupe_NroDocumento(String supe_NroDocumento) 
	{
		Supe_NroDocumento = supe_NroDocumento;
	}
	
	public String getSupe_TokenFirebase() 
	{
		return Supe_TokenFirebase;
	}
	public void setSupe_TokenFirebase(String supe_TokenFirebase) 
	{
		Supe_TokenFirebase = supe_TokenFirebase;
	}
	
	public Boolean getSupe_Activo() 
	{
		return Supe_Activo;
	}
	public void setSupe_Activo(Boolean supe_Activo) 
	{
		Supe_Activo = supe_Activo;
	}
	
	public int getSupe_UsuarioCreacion() 
	{
		return Supe_UsuarioCreacion;
	}
	public void setSupe_UsuarioCreacion(int supe_UsuarioCreacion) 
	{
		Supe_UsuarioCreacion = supe_UsuarioCreacion;
	}
	
	public int getSupe_UsuarioEdicion() 
	{
		return Supe_UsuarioEdicion;
	}
	public void setSupe_UsuarioEdicion(int supe_UsuarioEdicion) 
	{
		Supe_UsuarioEdicion = supe_UsuarioEdicion;
	}
	
	public Timestamp getSupe_FechaCreacion() 
	{
		return Supe_FechaCreacion;
	}
	public void setSupe_FechaCreacion(Timestamp supe_FechaCreacion) 
	{
		Supe_FechaCreacion = supe_FechaCreacion;
	}
	
	public Timestamp getSupe_FechaEdicion() 
	{
		return Supe_FechaEdicion;
	}
	public void setSupe_FechaEdicion(Timestamp supe_FechaEdicion) 
	{
		Supe_FechaEdicion = supe_FechaEdicion;
	}
	
	public Supervisor toSupervisor() {
		Supervisor supervisor = new Supervisor();
        BeanUtils.copyProperties(this, supervisor);
        return supervisor;
    }
}
