package app.parqueo.infrastructure.postgresql.entities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Transaccion\"")
public class TransaccionEntity{
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private BigInteger Tran_Id;
	private Integer Clie_Id;
	private char Tran_Tipo;
	private BigDecimal Tran_Monto;
	
	
	private Integer Frec_Id;
	private Integer Tarj_Id;
	private Timestamp Tran_FechaCreacion;
	private Integer Tran_UsuarioCreacion;
	private BigDecimal Tran_SaldoActual;	
		
	public BigInteger getTran_Id() {
		return Tran_Id;
	}
	public void setTran_Id(BigInteger tran_Id) {
		Tran_Id = tran_Id;
	}
	public Integer getClie_Id() {
		return Clie_Id;
	}
	public void setClie_Id(Integer clie_Id) {
		Clie_Id = clie_Id;
	}
	public char getTran_Tipo() {
		return Tran_Tipo;
	}
	public void setTran_Tipo(char tran_Tipo) {
		Tran_Tipo = tran_Tipo;
	}
	public BigDecimal getTran_Monto() {
		return Tran_Monto;
	}
	public void setTran_Monto(BigDecimal tran_Monto) {
		Tran_Monto = tran_Monto;
	}
	
	public Integer getFrec_Id() {
		return Frec_Id;
	}
	public void setFrec_Id(Integer frec_Id) {
		Frec_Id = frec_Id;
	}
	public Integer getTarj_Id() {
		return Tarj_Id;
	}
	public void setTarj_Id(Integer tarj_Id) {
		Tarj_Id = tarj_Id;
	}
	public Timestamp getTran_FechaCreacion() {
		return Tran_FechaCreacion;
	}
	public void setTran_FechaCreacion(Timestamp tran_FechaCreacion) {
		Tran_FechaCreacion = tran_FechaCreacion;
	}
	public Integer getTran_UsuarioCreacion() {
		return Tran_UsuarioCreacion;
	}
	public void setTran_UsuarioCreacion(Integer tran_UsuarioCreacion) {
		Tran_UsuarioCreacion = tran_UsuarioCreacion;
	}
	public BigDecimal getTran_SaldoActual() {
		return Tran_SaldoActual;
	}
	public void setTran_SaldoActual(BigDecimal tran_SaldoActual) {
		Tran_SaldoActual = tran_SaldoActual;
	}
}



