package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Municipio;

@Entity
@Table(name="\"Municipio\"")
public class MunicipioEntity {

	@Id
	private Integer Muni_Id;
	private String Muni_Nombre;
	private Boolean Muni_Activo;
	private Integer Muni_UsuarioCreacion;
	private Integer Muni_UsuarioEdicion;
	private Timestamp Muni_FechaCreacion;
	private Timestamp Muni_FechaEdicion;

	public Integer getMuni_Id() {
		return Muni_Id;
	}
	public void setMuni_Id(Integer muni_Id) {
		Muni_Id = muni_Id;
	}
	public String getMuni_Nombre() {
		return Muni_Nombre;
	}
	public void setMuni_Nombre(String muni_Nombre) {
		Muni_Nombre = muni_Nombre;
	}
	public Boolean getMuni_Activo() {
		return Muni_Activo;
	}
	public void setMuni_Activo(Boolean muni_Activo) {
		Muni_Activo = muni_Activo;
	}
	public Integer getMuni_UsuarioCreacion() {
		return Muni_UsuarioCreacion;
	}
	public void setMuni_UsuarioCreacion(Integer muni_UsuarioCreacion) {
		Muni_UsuarioCreacion = muni_UsuarioCreacion;
	}
	public Integer getMuni_UsuarioEdicion() {
		return Muni_UsuarioEdicion;
	}
	public void setMuni_UsuarioEdicion(Integer muni_UsuarioEdicion) {
		Muni_UsuarioEdicion = muni_UsuarioEdicion;
	}
	public Timestamp getMuni_FechaCreacion() {
		return Muni_FechaCreacion;
	}
	public void setMuni_FechaCreacion(Timestamp muni_FechaCreacion) {
		Muni_FechaCreacion = muni_FechaCreacion;
	}
	public Timestamp getMuni_FechaEdicion() {
		return Muni_FechaEdicion;
	}
	public void setMuni_FechaEdicion(Timestamp muni_FechaEdicion) {
		Muni_FechaEdicion = muni_FechaEdicion;
	}
	
	public Municipio toMunicipio() {
		Municipio municipio = new Municipio();
        BeanUtils.copyProperties(this, municipio);
        return municipio;
    }
}
