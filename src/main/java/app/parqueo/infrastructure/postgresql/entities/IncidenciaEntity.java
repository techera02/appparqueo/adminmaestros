package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.Incidencia;

@Entity
@Table(name="\"Incidencia\"")
public class IncidenciaEntity {
	
	@Id
	private Integer Inci_Id;
	private Integer Tinc_Id;
	private Integer Supe_Id;
	private Integer Clie_Id;
	private Timestamp Inci_FechaCreacion;
	private String Inci_Descripcion;
	private String Inci_Placa;
	
	public Integer getInci_Id() {
		return Inci_Id;
	}
	public void setInci_Id(Integer inci_Id) {
		Inci_Id = inci_Id;
	}
	public Integer getTinc_Id() {
		return Tinc_Id;
	}
	public void setTinc_Id(Integer tinc_Id) {
		Tinc_Id = tinc_Id;
	}
	public Integer getSupe_Id() {
		return Supe_Id;
	}
	public void setSupe_Id(Integer supe_Id) {
		Supe_Id = supe_Id;
	}
	public Integer getClie_Id() {
		return Clie_Id;
	}
	public void setClie_Id(Integer clie_Id) {
		Clie_Id = clie_Id;
	}
	public Timestamp getInci_FechaCreacion() {
		return Inci_FechaCreacion;
	}
	public void setInci_FechaCreacion(Timestamp inci_FechaCreacion) {
		Inci_FechaCreacion = inci_FechaCreacion;
	}
	public String getInci_Descripcion() {
		return Inci_Descripcion;
	}
	public void setInci_Descripcion(String inci_Descripcion) {
		Inci_Descripcion = inci_Descripcion;
	}
	public String getInci_Placa() {
		return Inci_Placa;
	}
	public void setInci_Placa(String inci_Placa) {
		Inci_Placa = inci_Placa;
	}
	
	public Incidencia toIncidencia() {
		Incidencia incidencia = new Incidencia();
        BeanUtils.copyProperties(this, incidencia);
        return incidencia;
    }
}
