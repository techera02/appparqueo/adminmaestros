package app.parqueo.infrastructure.postgresql.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"TipoDocumento\"")
public class TipoDocumentoEntity {

	@Id
	private int Tdoc_Id;
	private String Tdoc_Nombre;
	
	public int getTdoc_Id() {
		return Tdoc_Id;
	}
	public void setTdoc_Id(int tdoc_Id) {
		Tdoc_Id = tdoc_Id;
	}
	public String getTdoc_Nombre() {
		return Tdoc_Nombre;
	}
	public void setTdoc_Nombre(String tdoc_Nombre) {
		Tdoc_Nombre = tdoc_Nombre;
	}
}
