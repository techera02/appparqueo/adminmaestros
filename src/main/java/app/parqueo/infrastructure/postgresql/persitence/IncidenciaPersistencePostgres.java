package app.parqueo.infrastructure.postgresql.persitence;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.IncidenciaFoto;
import app.parqueo.domain.model.ResponseIncidencia;
import app.parqueo.domain.model.ResponseIncidencia2;
import app.parqueo.domain.persistence_ports.IncidenciaPersistence;
import app.parqueo.infrastructure.postgresql.entities.IncidenciaFotoEntity;
import app.parqueo.infrastructure.repositories.IncidenciaFotoRepository;
import app.parqueo.infrastructure.repositories.IncidenciaRepository;

@Repository("IncidenciaPersistence")
public class IncidenciaPersistencePostgres implements IncidenciaPersistence
{
	private final IncidenciaRepository incidenciaRepository;
	private final IncidenciaFotoRepository incidenciaFotoRepository;
	
    @Autowired
    public IncidenciaPersistencePostgres(IncidenciaRepository incidenciaRepository, IncidenciaFotoRepository incidenciaFotoRepository) 
    {
        this.incidenciaRepository = incidenciaRepository;
        this.incidenciaFotoRepository = incidenciaFotoRepository;
    }

	@Override
	public List<ResponseIncidencia> listarIncidencia(Integer pagenumber, Integer pagesize, 
			Date fechainicio, Integer indiceFechainicio, 
			Date fechafin,Integer indiceFechafin,
			Integer tipoincidencia, String placa)
	{
		List<Object[]> list = this.incidenciaRepository.listarIncidencia(pagenumber,pagesize, fechainicio,indiceFechainicio,
				fechafin,indiceFechafin, tipoincidencia , placa);
		List<ResponseIncidencia> listaIncidencia = new ArrayList<ResponseIncidencia>();
		
		for(int x = 0;x<list.size();x++) {
			ResponseIncidencia entity = new ResponseIncidencia();
			entity.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entity.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entity.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entity.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));
			entity.setInci_Id(Integer.parseInt(list.get(x)[4].toString()));
			entity.setTinc_Id(Integer.parseInt(list.get(x)[5].toString()));
			entity.setSupe_Id(Integer.parseInt(list.get(x)[6].toString()));
			entity.setClie_Id(Integer.parseInt(list.get(x)[7].toString()));
			entity.setInci_FechaCreacion(Timestamp.valueOf(list.get(x)[8].toString()));
			entity.setInci_Descripcion(list.get(x)[9].toString());
			entity.setInci_Placa(list.get(x)[10].toString());
			entity.setTinc_Nombre(list.get(x)[11].toString());
			
			listaIncidencia.add(entity);
		}
		return listaIncidencia;
	}

	@Override
	public Integer validarIncidenciaExiste(Integer idincidencia) {
		return this.incidenciaRepository.validarIncidenciaExiste(idincidencia);
	}
	
	@Override
	public ResponseIncidencia2 seleccionarIncidenciaFoto(Integer idincidencia) {
		
		ResponseIncidencia2 response = new ResponseIncidencia2();
		List<Object[]> list = this.incidenciaRepository.seleccionarIncidencia(idincidencia);
		
		for(int x = 0;x<list.size();x++) {
			response.setInci_Id(Integer.parseInt(list.get(x)[0].toString()));
			response.setTinc_Id(Integer.parseInt(list.get(x)[1].toString()));
			response.setSupe_Id(Integer.parseInt(list.get(x)[2].toString()));
			response.setClie_Id(Integer.parseInt(list.get(x)[3].toString()));
			response.setInci_FechaCreacion(Timestamp.valueOf(list.get(x)[4].toString()));
			response.setInci_Descripcion(list.get(x)[5].toString());
			response.setInci_Placa(list.get(x)[6].toString());
			response.setTinc_Nombre(list.get(x)[7].toString());
		}
		
		List<IncidenciaFotoEntity> list2 = this.incidenciaFotoRepository.listarIncidenciaFoto(idincidencia);
		List<IncidenciaFoto> listarIncidenciaFoto = new ArrayList<IncidenciaFoto>();
		
		for(int x = 0;x<list2.size();x++) {
			IncidenciaFoto entidad = new IncidenciaFoto();
			entidad.setIfot_Id(list2.get(x).getIfot_Id());
			entidad.setInci_Id(list2.get(x).getInci_Id());
			entidad.setIfot_NombreArchivo(list2.get(x).getIfot_NombreArchivo());
			
			listarIncidenciaFoto.add(entidad);
		}
		
		response.setFotos(listarIncidenciaFoto);
		
		return response;
	}
	
}