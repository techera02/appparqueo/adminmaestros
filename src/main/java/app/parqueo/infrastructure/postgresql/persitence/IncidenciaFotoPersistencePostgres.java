package app.parqueo.infrastructure.postgresql.persitence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.IncidenciaFoto;
import app.parqueo.domain.persistence_ports.IncidenciaFotoPersistence;
import app.parqueo.infrastructure.postgresql.entities.IncidenciaFotoEntity;
import app.parqueo.infrastructure.repositories.IncidenciaFotoRepository;

@Repository("IncidenciaFotoPersistence")
public class IncidenciaFotoPersistencePostgres implements IncidenciaFotoPersistence{

	private final IncidenciaFotoRepository incidenciaFotoRepository;
	
    @Autowired
    public IncidenciaFotoPersistencePostgres(IncidenciaFotoRepository incidenciaFotoRepository) 
    {
        this.incidenciaFotoRepository = incidenciaFotoRepository;
    }
	
    @Override
	public List<IncidenciaFoto> listarIncidenciaFoto(Integer idincidencia) {
		List<IncidenciaFotoEntity> list = this.incidenciaFotoRepository.listarIncidenciaFoto(idincidencia);
		List<IncidenciaFoto> listarIncidenciaFoto = new ArrayList<IncidenciaFoto>();
		
		for(int x = 0;x<list.size();x++) {
			IncidenciaFoto entidad = new IncidenciaFoto();
			entidad.setIfot_Id(list.get(x).getIfot_Id());
			entidad.setInci_Id(list.get(x).getInci_Id());
			entidad.setIfot_NombreArchivo(list.get(x).getIfot_NombreArchivo());
			
			listarIncidenciaFoto.add(entidad);
		}
		return listarIncidenciaFoto;
	}
}
