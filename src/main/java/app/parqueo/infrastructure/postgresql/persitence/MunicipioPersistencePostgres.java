package app.parqueo.infrastructure.postgresql.persitence;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Municipio;
import app.parqueo.domain.model.ResponseMunicipio;
import app.parqueo.domain.persistence_ports.MunicipioPersistence;
import app.parqueo.infrastructure.repositories.MunicipioRepository;

@Repository("MunicipioPersistence")
public class MunicipioPersistencePostgres implements MunicipioPersistence {

	
	private final MunicipioRepository municipioRepository;
	
    @Autowired
    public MunicipioPersistencePostgres(MunicipioRepository municipioRepository) 
    {
        this.municipioRepository = municipioRepository;
    }

	@Override
	public List<ResponseMunicipio> listarMunicipio(Integer pagenumber, Integer pagesize, String nombre, Integer activo) {
		List<Object[]> list = this.municipioRepository.listarMunicipio(pagenumber,pagesize,nombre,activo);
		List<ResponseMunicipio> listaMunicipio = new ArrayList<ResponseMunicipio>();
		
		for(int x = 0;x<list.size();x++) {
			ResponseMunicipio entity = new ResponseMunicipio();
			entity.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entity.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entity.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entity.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));
			entity.setMuni_Id(Integer.parseInt(list.get(x)[4].toString()));
			entity.setMuni_Nombre(list.get(x)[5].toString());
			entity.setMuni_Activo(Boolean.parseBoolean(list.get(x)[6].toString()));
			entity.setMuni_UsuarioCreacion(Integer.parseInt(list.get(x)[7].toString()));
			entity.setMuni_UsuarioEdicion((list.get(x)[8] == null) ? 0 : Integer.parseInt(list.get(x)[8].toString()));
			entity.setMuni_FechaCreacion(Timestamp.valueOf(list.get(x)[9].toString()));
			entity.setMuni_FechaEdicion((list.get(x)[10] == null) ? null : Timestamp.valueOf(list.get(x)[10].toString()));
			entity.setTari_Id(Integer.parseInt(list.get(x)[11].toString()));
			entity.setTari_Monto(new BigDecimal(list.get(x)[12].toString()));
			
			listaMunicipio.add(entity);
		}
		return listaMunicipio;
	}
    
	@Override
	public Integer insertarMunicipio(Municipio municipio) 
	{
		return this.municipioRepository.insertarMunicipio(municipio.getMuni_Nombre(), municipio.getMuni_Activo(), municipio.getMuni_UsuarioCreacion());
	}
	
	@Override
	public Integer actualizarMunicipio(Municipio municipio) {
		return this.municipioRepository.actualizarMunicipio(municipio.getMuni_Id(),municipio.getMuni_Nombre(),municipio.getMuni_Activo(),
				municipio.getMuni_UsuarioEdicion());
	}

	@Override
	public Integer verificarNombre(String muni_nombre, Integer idMunicipio) 
	{
		return this.municipioRepository.validarNombreMunicipio(muni_nombre, idMunicipio);
	}
    
	@Override
	public Integer validarMunicipioExiste(Integer idmunicipio) {
		return this.municipioRepository.validarMunicipioExiste(idmunicipio);
	}
	
	@Override
	public Municipio seleccionarMunicipio(Integer idmunicipio) {
		return this.municipioRepository.seleccionarMunicipio(idmunicipio).toMunicipio();
	}
	
	@Override
	public Integer eliminarMunicipio(Integer idmunicipio,  Integer idtarifa) {
		return this.municipioRepository.eliminarMunicipio(idmunicipio, idtarifa);
	}

	@Override
	public Integer validarMunicipioDependencias(Integer idmunicipio) {
		return this.municipioRepository.validarMunicipioDependencias(idmunicipio);
	}
}
