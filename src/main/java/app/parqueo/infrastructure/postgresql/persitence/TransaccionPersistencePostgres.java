package app.parqueo.infrastructure.postgresql.persitence;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import app.parqueo.domain.persistence_ports.TransaccionPersistence;
import app.parqueo.infrastructure.repositories.TransaccionRepository;

@Repository("TransaccionPersistence")
public class TransaccionPersistencePostgres implements TransaccionPersistence
{

	private final TransaccionRepository transaccionRepository;
	
    @Autowired
    public TransaccionPersistencePostgres(TransaccionRepository transaccionRepository) 
    {
        this.transaccionRepository = transaccionRepository;
    }
    
	@Override
	public Integer recargarBilletera(Integer clie_id, BigDecimal monto, Integer frec_id, Integer tarj_id) {
		return this.transaccionRepository.recargarBilletera( clie_id,  monto,  frec_id,  tarj_id);
	}

	@Override
	public Integer validarRecargaBilletera(Integer clie_id, Integer frec_id, Integer tarj_id) {
		return this.transaccionRepository.ValidarRecargaBilletera(clie_id, frec_id, tarj_id);
	}

}

