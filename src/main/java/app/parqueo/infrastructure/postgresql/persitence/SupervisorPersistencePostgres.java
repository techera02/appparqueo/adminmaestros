package app.parqueo.infrastructure.postgresql.persitence;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ResponseSupervisor;
import app.parqueo.domain.model.Supervisor;
import app.parqueo.domain.persistence_ports.SupervisorPersistence;
import app.parqueo.infrastructure.repositories.SupervisorRepository;

@Repository("SupervisorPersistence")
public class SupervisorPersistencePostgres implements SupervisorPersistence {

	private final SupervisorRepository supervisorRepository;

	@Autowired
	public SupervisorPersistencePostgres(SupervisorRepository supervisorRepository) {
		this.supervisorRepository = supervisorRepository;
	}

	@Override
	public Integer insertarSupervisor(Supervisor supervisor) {
		return this.supervisorRepository.insertarSupervisor(supervisor.getSupe_Nombres(),
				supervisor.getSupe_ApellidoPaterno(), supervisor.getSupe_ApellidoMaterno(), supervisor.getTdoc_Id(),
				supervisor.getSupe_NroDocumento(), supervisor.getSupe_TokenFirebase(), supervisor.getSupe_Activo(),
				supervisor.getSupe_UsuarioCreacion(), supervisor.getSupe_Clave(), supervisor.getSupe_Alias(),
				supervisor.getSupe_Correo());
	}

	@Override
	public Integer actualizarSupervisor(Supervisor supervisor) {
		return this.supervisorRepository.actualizarSupervisor(supervisor.getSupe_Nombres(),
				supervisor.getSupe_ApellidoPaterno(), supervisor.getSupe_ApellidoMaterno(), supervisor.getTdoc_Id(),
				supervisor.getSupe_NroDocumento(), supervisor.getSupe_TokenFirebase(), supervisor.getSupe_Activo(),
				supervisor.getSupe_UsuarioEdicion(), supervisor.getSupe_Correo(), supervisor.getSupe_Id());
	}

	@Override
	public Integer verificarCorreo(String correo, Integer idSupervisor) {
		return this.supervisorRepository.validarCorreoSupervisor(correo, idSupervisor);
	}

	@Override
	public Integer validarAliasSupervisor(String supe_alias) {
		return this.supervisorRepository.validarAliasSupervisor(supe_alias);
	}

	@Override
	public List<ResponseSupervisor> listarSupervisor(Integer pagenumber, Integer pagesize, String nombre,
			String documento, Integer activo) {
		List<Object[]> list = this.supervisorRepository.listarSupervisor(pagenumber,pagesize,nombre,documento,activo);
		List<ResponseSupervisor> listaSupervisor = new ArrayList<ResponseSupervisor>();

		for (int x = 0; x < list.size(); x++) {
			ResponseSupervisor entity = new ResponseSupervisor();
			entity.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entity.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entity.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entity.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));
			entity.setSupe_Id(Integer.parseInt(list.get(x)[4].toString()));
			entity.setSupe_Nombres(list.get(x)[5].toString());
			entity.setSupe_ApellidoPaterno(list.get(x)[6].toString());
			entity.setSupe_ApellidoMaterno(list.get(x)[7].toString());
			entity.setTdoc_Id(Integer.parseInt(list.get(x)[8].toString()));
			entity.setSupe_NroDocumento(list.get(x)[9].toString());
			entity.setSupe_TokenFirebase(list.get(x)[10].toString());
			entity.setSupe_Activo(Boolean.parseBoolean(list.get(x)[11].toString()));
			entity.setSupe_UsuarioCreacion(Integer.parseInt(list.get(x)[12].toString()));
			entity.setSupe_UsuarioEdicion((list.get(x)[13] == null) ? 0 : Integer.parseInt(list.get(x)[13].toString()));
			entity.setSupe_FechaCreacion(Timestamp.valueOf(list.get(x)[14].toString()));
			entity.setSupe_FechaEdicion(
					(list.get(x)[15] == null) ? null : Timestamp.valueOf(list.get(x)[15].toString()));
			entity.setSupe_Clave(list.get(x)[16].toString());
			entity.setSupe_Alias(list.get(x)[17].toString());
			entity.setSupe_Correo(list.get(x)[18].toString());

			listaSupervisor.add(entity);
		}
		return listaSupervisor;
	}

	@Override
	public Integer validarSupervisorExiste(Integer idsupervisor) {
		return this.supervisorRepository.validarSupervisorExiste(idsupervisor);
	}
	
	@Override
	public Integer eliminarSupervisor(Integer idsupervisor) {
		return this.supervisorRepository.eliminarSupervisor(idsupervisor);
	}
	
	@Override
	public Integer validarDocumento(String documento, Integer idSupervisor) {
		return this.supervisorRepository.validarDocumentoSupervisor(documento, idSupervisor);
	}
	
	@Override
	public Supervisor seleccionarSupervisor(Integer idsupervisor) {
		return this.supervisorRepository.seleccionarSupervisor(idsupervisor).toSupervisor();
	}
	
	@Override
	public Integer recuperarContraseniaSupervisor(String correo,String clave) {
		return this.supervisorRepository.recuperarContraseniaSupervisor(correo,clave);
	}
	
	@Override
	public Supervisor seleccionarSupervisorPorCorreo(String correo) {
		return this.supervisorRepository.seleccionarSupervisorPorCorreo(correo).toSupervisor();
	}
}
