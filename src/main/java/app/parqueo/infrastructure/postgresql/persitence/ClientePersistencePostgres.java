package app.parqueo.infrastructure.postgresql.persitence;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.ResponseCliente;
import app.parqueo.domain.model.ResponseDetalleCliente;
import app.parqueo.domain.persistence_ports.ClientePersistence;
import app.parqueo.infrastructure.repositories.ClienteRepository;

@Repository("ClientePersistence")
public class ClientePersistencePostgres implements ClientePersistence {

	
	private final ClienteRepository clienteRepository;
	
    @Autowired
    public ClientePersistencePostgres(ClienteRepository clienteRepository) 
    {
        this.clienteRepository = clienteRepository;
    }

	@Override
	public List<ResponseCliente> listarCliente(Integer pagenumber, Integer pagesize, String nombre,
			String nrodocumento,String placa) {
		List<Object[]> list = this.clienteRepository.listarCliente(pagenumber,pagesize, nombre,
				 nrodocumento, placa);
		List<ResponseCliente> listaCliente = new ArrayList<ResponseCliente>();
		
		for(int x = 0;x<list.size();x++) {
			ResponseCliente entity = new ResponseCliente();
			entity.setRowNumber(Integer.parseInt(list.get(x)[0].toString()));
			entity.setRowCount(Integer.parseInt(list.get(x)[1].toString()));
			entity.setPageCount(Integer.parseInt(list.get(x)[2].toString()));
			entity.setPageIndex(Integer.parseInt(list.get(x)[3].toString()));			
			entity.setClie_Id(Integer.parseInt(list.get(x)[4].toString()));
			entity.setClie_Nombres(list.get(x)[5].toString());
			entity.setClie_ApePaterno(list.get(x)[6].toString());
			entity.setClie_ApeMaterno(list.get(x)[7].toString());
			entity.setTdoc_Id((list.get(x)[8] == null) ? 0 : Integer.parseInt(list.get(x)[8].toString()));
			entity.setClie_NroDocumento((list.get(x)[9] == null) ? null : list.get(x)[9].toString());
			entity.setClie_TokenFirebase((list.get(x)[10] == null) ? null : list.get(x)[10].toString());
			entity.setClie_FechaCreacion(Timestamp.valueOf(list.get(x)[11].toString()));
			entity.setClie_FechaEdicion((list.get(x)[12] == null) ? null : Timestamp.valueOf(list.get(x)[12].toString()));
			entity.setClie_NroTelefono((list.get(x)[13] == null) ? null : list.get(x)[13].toString());
			entity.setClie_Correo((list.get(x)[14] == null) ? null : list.get(x)[14].toString());
			entity.setTdoc_Nombre((list.get(x)[15] == null) ? null : list.get(x)[15].toString());
			entity.setVehi_Placa((list.get(x)[16] == null) ? null : list.get(x)[16].toString());
			entity.setSaldo((list.get(x)[17] == null) ? null : new BigDecimal(list.get(x)[17].toString()));
			
			listaCliente.add(entity);
		}
		return listaCliente;
	}
    
	@Override
	public Integer validarClienteExiste(Integer idcliente) {
		return this.clienteRepository.validarClienteExiste(idcliente);
	}
	
	@Override
	public ResponseDetalleCliente seleccionarDetalleCliente(Integer idcliente) {
		
		List<Object[]> list = this.clienteRepository.seleccionarDetalleCliente(idcliente);
		ResponseDetalleCliente detalleCliente  = new ResponseDetalleCliente();	
		
		for(int x = 0;x<list.size();x++) {
			detalleCliente.setClie_Id(Integer.parseInt(list.get(x)[0].toString()));
			detalleCliente.setClie_Nombres(list.get(x)[1].toString());
			detalleCliente.setClie_ApePaterno(list.get(x)[2].toString());
			detalleCliente.setClie_ApeMaterno(list.get(x)[3].toString());
			detalleCliente.setTdoc_Id((list.get(x)[4] == null) ? 0 : Integer.parseInt(list.get(x)[4].toString()));
			detalleCliente.setClie_NroDocumento((list.get(x)[5] == null) ? null : list.get(x)[5].toString());
			detalleCliente.setClie_TokenFirebase((list.get(x)[6] == null) ? null : list.get(x)[6].toString());
			detalleCliente.setClie_FechaCreacion(Timestamp.valueOf(list.get(x)[7].toString()));
			detalleCliente.setClie_FechaEdicion((list.get(x)[8] == null) ? null : Timestamp.valueOf(list.get(x)[8].toString()));
			detalleCliente.setClie_NroTelefono((list.get(x)[9] == null) ? null : list.get(x)[9].toString());
			detalleCliente.setClie_Correo((list.get(x)[10] == null) ? null : list.get(x)[10].toString());
			detalleCliente.setTdoc_Nombre((list.get(x)[11] == null) ? null : list.get(x)[11].toString());
			detalleCliente.setSaldo((list.get(x)[12] == null) ? null : new BigDecimal(list.get(x)[12].toString()));
		}
		
		return detalleCliente;
	}
	
}

