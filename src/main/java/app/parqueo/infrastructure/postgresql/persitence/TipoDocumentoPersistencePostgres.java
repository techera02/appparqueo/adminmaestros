package app.parqueo.infrastructure.postgresql.persitence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.TipoDocumento;
import app.parqueo.domain.persistence_ports.TipoDocumentoPersistence;
import app.parqueo.infrastructure.postgresql.entities.TipoDocumentoEntity;
import app.parqueo.infrastructure.repositories.TipoDocumentoRepository;

@Repository("TipoDocumentoPersistence")
public class TipoDocumentoPersistencePostgres implements TipoDocumentoPersistence{

	private final TipoDocumentoRepository tipoDocumentoRepository;
	
    @Autowired
    public TipoDocumentoPersistencePostgres(TipoDocumentoRepository tipoDocumentoRepository) 
    {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }
    
    @Override
	public List<TipoDocumento> listarTipoDocumento() {
		List<TipoDocumentoEntity> list = this.tipoDocumentoRepository.listarTipoDocumento();
		List<TipoDocumento> listarTipoDocumento = new ArrayList<TipoDocumento>();
		
		for(int x = 0;x<list.size();x++) {
			TipoDocumento entidad = new TipoDocumento();
			entidad.setTdoc_Id(list.get(x).getTdoc_Id());
			entidad.setTdoc_Nombre(list.get(x).getTdoc_Nombre());
			
			listarTipoDocumento.add(entidad);
		}
		return listarTipoDocumento;
	}
}
