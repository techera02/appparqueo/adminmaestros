package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;

import app.parqueo.application.EmailService;
import app.parqueo.application.SupervisorService;
import app.parqueo.domain.model.GenerarPassword;
import app.parqueo.domain.model.ResponseActualizarSupervisor;
import app.parqueo.domain.model.ResponseEliminarSupervisor;
import app.parqueo.domain.model.ResponseInsertarSupervisor;
import app.parqueo.domain.model.ResponseListarSupervisor;
import app.parqueo.domain.model.ResponseRecuperarContraseniaSupervisor;
import app.parqueo.domain.model.ResponseReenviarCredencialesSupervisor;
import app.parqueo.domain.model.Supervisor;

@RestController
@RequestMapping("/Supervisor")
public class SupervisorController {

	private final SupervisorService supervisorService;

	@Autowired
	public SupervisorController(SupervisorService supervisorService) {
		this.supervisorService = supervisorService;
	}

	@PostMapping("/InsertarSupervisor")
	public ResponseEntity<ResponseInsertarSupervisor> insertarSupervisor(@RequestBody Supervisor supervisor) {
		ResponseInsertarSupervisor response = new ResponseInsertarSupervisor();

		String nombres = supervisor.getSupe_Nombres();
		String apellidoP = supervisor.getSupe_ApellidoPaterno();
		String apellidoM = supervisor.getSupe_ApellidoMaterno();

		supervisor.setSupe_Nombres(formatoNombre(nombres));
		supervisor.setSupe_ApellidoPaterno(formatoNombre(apellidoP));
		supervisor.setSupe_ApellidoMaterno(formatoNombre(apellidoM));

		try {

			Integer temp = this.supervisorService.validarAliasSupervisor(supervisor.getSupe_Alias());

			if (temp == 0) {

				Integer temp1 = this.supervisorService.verificarCorreo(supervisor.getSupe_Correo(), 0);

				if (temp1 == 0) {

					Integer temp2 = this.supervisorService.validarDocumento(supervisor.getSupe_NroDocumento(), 0);

					if (temp2 == 0) {
						String clave = GenerarPassword.getRandomPassword(8);
						supervisor.setSupe_Clave(clave);
						Integer indice = this.supervisorService.insertarSupervisor(supervisor);

						if (indice > 0) {

							// variables
							Context context = new Context();
							context.setVariable("usuario",
									supervisor.getSupe_Nombres() + " " + supervisor.getSupe_ApellidoPaterno() + " "
											+ supervisor.getSupe_ApellidoMaterno());
							context.setVariable("alias", supervisor.getSupe_Alias());
							context.setVariable("clave", clave);

							EmailService.enviarMail(supervisor.getSupe_Correo(), "Acceso a la app de Supervisor",
									"CorreoRegistroSupervisor", context);

							response.setMensaje("Se han insertado los datos del supervisor");
							response.setEstado(true);
							response.setSupe_Id(indice);
						}

					} else {
						response.setCodError(3);
						response.setMensaje("El número de documento ya está registrado");
						response.setEstado(false);
					}
				} else {
					response.setCodError(2);
					response.setMensaje("El correo ya está registrado");
					response.setEstado(false);
				}

			} else {
				response.setCodError(1);
				response.setMensaje("El alias ya existe");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}

		return ResponseEntity.ok(response);
	}

	@PutMapping("/ActualizarSupervisor")
	public ResponseEntity<ResponseActualizarSupervisor> actualizarSupervisor(@RequestBody Supervisor supervisor) {
		ResponseActualizarSupervisor response = new ResponseActualizarSupervisor();
		response.setEstado(true);

		String nombres = supervisor.getSupe_Nombres();
		String apellidoP = supervisor.getSupe_ApellidoPaterno();
		String apellidoM = supervisor.getSupe_ApellidoMaterno();

		supervisor.setSupe_Nombres(formatoNombre(nombres));
		supervisor.setSupe_ApellidoPaterno(formatoNombre(apellidoP));
		supervisor.setSupe_ApellidoMaterno(formatoNombre(apellidoM));

		try {
			Integer temp = this.supervisorService.verificarCorreo(supervisor.getSupe_Correo(), supervisor.getSupe_Id());

			if (temp == 0) {

				Integer temp1 = this.supervisorService.validarDocumento(supervisor.getSupe_NroDocumento(),
						supervisor.getSupe_Id());

				if (temp1 == 0) {
					this.supervisorService.actualizarSupervisor(supervisor);
					response.setMensaje("Se han actualizado los datos del supervisor");
					response.setSupe_Id(supervisor.getSupe_Id());
				} else {
					response.setCodError(2);
					response.setMensaje("El número de documento ya está registrado");
					response.setEstado(false);
				}

			} else {
				response.setCodError(1);
				response.setMensaje("El correo ya está registrado");
				response.setEstado(false);
			}
		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlador");
			response.setEstado(false);

		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/ListarSupervisor/{pagenumber}/{pagesize}/{nombre}/{documento}/{activo}")
	public ResponseEntity<ResponseListarSupervisor> listarSupervisor(@PathVariable("pagenumber") Integer pagenumber,
			@PathVariable("pagesize") Integer pagesize, @PathVariable("nombre") String nombre,
			@PathVariable("documento") String documento, @PathVariable("activo") Integer activo) {
		ResponseListarSupervisor response = new ResponseListarSupervisor();
		response.setEstado(true);

		try {

			if (nombre.equals("*")) {
				nombre = "";
			}

			if (documento.equals("*")) {
				documento = "";
			}

			response.setLista(supervisorService.listarSupervisor(pagenumber, pagesize, nombre, documento, activo));
		} catch (Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje(e.getMessage());
		}

		return ResponseEntity.ok(response);
	}

	@DeleteMapping("/EliminarSupervisor/{idsupervisor}")
	public ResponseEntity<ResponseEliminarSupervisor> eliminarSupervisor(
			@PathVariable("idsupervisor") Integer idsupervisor) {
		ResponseEliminarSupervisor response = new ResponseEliminarSupervisor();
		response.setEstado(true);

		try {

			Integer temp = this.supervisorService.validarSupervisorExiste(idsupervisor);

			if (temp == 1) {
				this.supervisorService.eliminarSupervisor(idsupervisor);
				response.setResultado(true);
			} else {
				response.setCodError(1);
				response.setMensaje("Supervisor no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}

	@GetMapping("/ReenviarCredencialesSupervisor/{idsupervisor}")
	public ResponseEntity<ResponseReenviarCredencialesSupervisor> reenviarCredencialesSupervisor(
			@PathVariable("idsupervisor") Integer idsupervisor) {
		ResponseReenviarCredencialesSupervisor response = new ResponseReenviarCredencialesSupervisor();
		response.setEstado(true);
		response.setResultado(false);

		try {

			Integer temp = this.supervisorService.validarSupervisorExiste(idsupervisor);

			if (temp == 1) {

				Supervisor supervisor = this.supervisorService.seleccionarSupervisor(idsupervisor);

				// variables
				Context context = new Context();
				context.setVariable("usuario", supervisor.getSupe_Nombres() + " " + supervisor.getSupe_ApellidoPaterno()
						+ " " + supervisor.getSupe_ApellidoMaterno());
				context.setVariable("alias", supervisor.getSupe_Alias());
				context.setVariable("clave", supervisor.getSupe_Clave());

				EmailService.enviarMail(supervisor.getSupe_Correo(), "Acceso a la app de Supervisor",
						"CorreoRegistroSupervisor", context);

				response.setResultado(true);

			} else {
				response.setCodError(1);
				response.setMensaje("Supervisor no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/RecuperarContraseniaSupervisor")
	public ResponseEntity<ResponseRecuperarContraseniaSupervisor> recuperarContraseniaSupervisor(@RequestBody Supervisor supervisor) {
		ResponseRecuperarContraseniaSupervisor response = new ResponseRecuperarContraseniaSupervisor();
		response.setEstado(true);
		response.setResultado(false);

		try {
			Integer temp = this.supervisorService.verificarCorreo(supervisor.getSupe_Correo(), 0);

			if (temp == 1) {
				String clave = GenerarPassword.getRandomPassword(8);
				this.supervisorService.recuperarContraseniaSupervisor(supervisor.getSupe_Correo(), clave);

				Supervisor supervisorBE = this.supervisorService.seleccionarSupervisorPorCorreo(supervisor.getSupe_Correo());

				// variables
				Context context = new Context();
				context.setVariable("usuario", supervisorBE.getSupe_Nombres() + " " + supervisorBE.getSupe_ApellidoPaterno()
				+ " " + supervisorBE.getSupe_ApellidoMaterno());
				context.setVariable("alias", supervisorBE.getSupe_Alias());
				context.setVariable("clave", clave);

				EmailService.enviarMail(supervisor.getSupe_Correo(), "Recuperar contraseña de la app Supervisor",
						"CorreoRecuperarContraseniaSupervisor", context);
				response.setResultado(true);

			} else {
				response.setCodError(1);
				response.setMensaje("El correo del supervisor no existe");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado" + e);
			response.setEstado(false);
		}

		return ResponseEntity.ok(response);
	}

	public String formatoNombre(String dato) {

		dato = dato.toLowerCase();
		char[] arr = dato.trim().toCharArray();

		for (int x = 0; x < dato.length(); x++) {
			if (x == 0) {
				arr[x] = Character.toUpperCase(arr[x]);
			}

			if (arr[x] == ' ') {
				arr[x + 1] = Character.toUpperCase(arr[x + 1]);
			}
		}
		return new String(arr);
	}
}
