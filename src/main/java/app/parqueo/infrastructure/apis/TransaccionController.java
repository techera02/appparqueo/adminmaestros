package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.ClienteService;
import app.parqueo.application.TransaccionService;
import app.parqueo.application.TransaccionValidator;
import app.parqueo.domain.model.ResponseError;
import app.parqueo.domain.model.Transaccion;

@RestController
@RequestMapping("/Transaccion")
public class TransaccionController 
{
	private final TransaccionService transaccionService;
	
	@Autowired
    public TransaccionController(TransaccionService transaccionService, ClienteService clienteService) 
	{
        this.transaccionService = transaccionService;

    }
	

	@PostMapping("/RecargarBilletera")
    public ResponseEntity<ResponseError> recargarBilletera
    (@RequestBody Transaccion transaccion) 
	{  
		ResponseError response = new ResponseError();
		TransaccionValidator transaccionValidator= new TransaccionValidator();
		
		response = transaccionValidator.recargarBilletera(transaccion);
		
		if(response.getEstado() == true) {
			
			if (transaccion.getTarj_Id() == null) {
				transaccion.setTarj_Id(0);
			}
			
			Integer temp = this.transaccionService.validarRecargaBilletera(transaccion.getClie_Id(), transaccion.getFrec_Id(), 
					transaccion.getTarj_Id());
			
			if (temp == 0) {
			
				try {
					this.transaccionService.recargarBilletera(transaccion.getClie_Id()
						,transaccion.getTran_Monto(), transaccion.getFrec_Id(), transaccion.getTarj_Id());
				}catch(Exception e) {
					response.setCodError(0);
					response.setMensaje("Error no controlado");
					response.setEstado(false);
				}
			}else {
				response.setCodError(temp);
				response.setEstado(false);
				if(temp == 4) {
					response.setMensaje("El cliente no esta registrado");
				}else if (temp == 5) {
					response.setMensaje("La forma de pago no existe");
				}else if (temp == 6) {
					response.setMensaje("La tarjeta no esta registrado");
				}
			}
		}
		
		return ResponseEntity.ok(response);
	}
	
	
}
