package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;

import app.parqueo.application.ClienteService;
import app.parqueo.domain.model.ResponseListarCliente;
import app.parqueo.domain.model.ResponseSeleccionarDetalleCliente;

@RestController
@RequestMapping("/Cliente")
public class ClienteController {

	private final ClienteService clienteService;

	@Autowired
	public ClienteController(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@GetMapping("/ListarCliente/{pagenumber}/{pagesize}/{nombre}/{indiceNombre}/{nrodocumento}/{indiceNrodocumento}"
			+ "/{placa}/{indicePlaca}")
	public ResponseEntity<ResponseListarCliente> listarCliente(
			@PathVariable("pagenumber") Integer pagenumber, @PathVariable("pagesize") Integer pagesize,
			@PathVariable("nombre") String nombre, @PathVariable("indiceNombre") Integer indiceNombre,
			@PathVariable("nrodocumento") String nrodocumento, @PathVariable("indiceNrodocumento") Integer indiceNrodocumento,
			@PathVariable("placa") String placa, @PathVariable("indicePlaca") Integer indicePlaca
			) {
		ResponseListarCliente response = new ResponseListarCliente();

		nombre = (indiceNombre==0)? "": nombre;
		nrodocumento = (indiceNrodocumento==0)? "": nrodocumento;
		placa = (indicePlaca==0)? "" : placa;
			
		try {
			response.setLista(clienteService.listarCliente(pagenumber, pagesize, nombre, nrodocumento, placa));

			for (int x = 0; x < response.getLista().size(); x++) {
				if (response.getLista().get(x).getTdoc_Nombre() == null) {
					response.getLista().get(x).setTdoc_Nombre("");
				}
				if (response.getLista().get(x).getClie_NroDocumento() == null) {
					response.getLista().get(x).setClie_NroDocumento("");
				}
				if (response.getLista().get(x).getSaldo() == null) {
					BigDecimal result = new BigDecimal(0);
					response.getLista().get(x).setSaldo(result);
				}
				if (response.getLista().get(x).getVehi_Placa() == null) {
					response.getLista().get(x).setVehi_Placa("");
				}
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje(e.getMessage());
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/SeleccionarDetalleCliente/{idcliente}")
	public ResponseEntity<ResponseSeleccionarDetalleCliente> seleccionarDetalleCliente(
			@PathVariable("idcliente") Integer idcliente) {
		ResponseSeleccionarDetalleCliente response = new ResponseSeleccionarDetalleCliente();
		response.setEstado(true);

		try {

			Integer temp = this.clienteService.validarClienteExiste(idcliente);

			if (temp == 1) {
				response.setDetalleCliente(this.clienteService.seleccionarDetalleCliente(idcliente));
			} else {
				response.setCodError(1);
				response.setMensaje("Cliente no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
}
