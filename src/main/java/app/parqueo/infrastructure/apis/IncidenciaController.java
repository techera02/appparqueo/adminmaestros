package app.parqueo.infrastructure.apis;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.IncidenciaService;
import app.parqueo.domain.model.ResponseListarIncidencia;
import app.parqueo.domain.model.ResponseSeleccionarIncidencia;

@RestController
@RequestMapping("/Incidencia")
public class IncidenciaController {

	private final IncidenciaService incidenciaService;

	@Autowired
	public IncidenciaController(IncidenciaService incidenciaService) {
		this.incidenciaService = incidenciaService;
	}

	@GetMapping("/ListarIncidencia/{pagenumber}/{pagesize}/{fechainicio}/{indiceFechainicio}"
			+ "/{fechafin}/{indiceFechaFin}/{tipoincidencia}/{placa}/{indicePlaca}")
	public ResponseEntity<ResponseListarIncidencia> listarIncidencia(
			@PathVariable("pagenumber") Integer pagenumber,
			@PathVariable("pagesize") Integer pagesize,
			@PathVariable("fechainicio") Date fechainicio,
			@PathVariable("indiceFechainicio") Integer indiceFechainicio,
			@PathVariable("fechafin") Date fechafin,
			@PathVariable("indiceFechaFin") Integer indiceFechaFin,
			@PathVariable("tipoincidencia") Integer tipoincidencia,
			@PathVariable("placa") String placa,
			@PathVariable("indicePlaca") Integer indicePlaca
			) {
		
		ResponseListarIncidencia response = new ResponseListarIncidencia();
		
		if(indicePlaca==0) {
			placa = "";
		}
				
		try {
			response.setLista(this.incidenciaService.listarIncidencia(pagenumber, pagesize,fechainicio,indiceFechainicio,
					fechafin,indiceFechaFin,tipoincidencia,placa));
		} catch (Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje(e.getMessage());
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/SeleccionarIncidencia/{idincidencia}")
	public ResponseEntity<ResponseSeleccionarIncidencia> seleccionarIncidencia(@PathVariable("idincidencia")Integer idincidencia) {
		ResponseSeleccionarIncidencia response = new ResponseSeleccionarIncidencia();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.incidenciaService.validarIncidenciaExiste(idincidencia);
			
			if(temp == 1) {				
				response.setIncidencia(this.incidenciaService.seleccionarIncidenciaFoto(idincidencia));
			}else {
				response.setCodError(1);
				response.setMensaje("Incidencia no registrado");
				response.setEstado(false);
			}			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
}
