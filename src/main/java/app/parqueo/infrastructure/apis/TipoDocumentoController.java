package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.TipoDocumentoService;
import app.parqueo.domain.model.ResponseListarTipoDocumento;

@RestController
@RequestMapping("/TipoDocumento")
public class TipoDocumentoController {

	private final TipoDocumentoService tipoDocumentoService;

	@Autowired
    public TipoDocumentoController(TipoDocumentoService tipoDocumentoService) 
	{
        this.tipoDocumentoService = tipoDocumentoService;
    }
	
	@GetMapping("/ListarTipoDocumento")
    public ResponseEntity<ResponseListarTipoDocumento> listarTipoDocumento()
	{  
		ResponseListarTipoDocumento response = new ResponseListarTipoDocumento();
		
			try {
				response.setLista(tipoDocumentoService.listarTipoDocumento());
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje(e.getMessage());
			}
			
		return ResponseEntity.ok(response);
    }
}
