package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.MunicipioService;
import app.parqueo.domain.model.Municipio;
import app.parqueo.domain.model.ResponseActualizarMunicipio;
import app.parqueo.domain.model.ResponseEliminarMunicipio;
import app.parqueo.domain.model.ResponseInsertarMunicipio;
import app.parqueo.domain.model.ResponseListarMunicipio;
import app.parqueo.domain.model.ResponseSeleccionarMunicipio;

@RestController
@RequestMapping("/Municipio")
public class MunicipioController {
	
	private final MunicipioService municipioService;
	
	@Autowired
    public MunicipioController(MunicipioService municipioService) 
	{
        this.municipioService = municipioService;
    }
	
	
	@GetMapping("/ListarMunicipio/{pagenumber}/{pagesize}/{nombre}/{activo}")
    public ResponseEntity<ResponseListarMunicipio> listarMunicipio(
    		@PathVariable("pagenumber")Integer pagenumber, @PathVariable("pagesize")Integer pagesize,
    		@PathVariable("nombre") String nombre, @PathVariable("activo") Integer activo)
	{  
		ResponseListarMunicipio response = new ResponseListarMunicipio();
		
			try {
				
				if(nombre.equals("*")) {
					nombre = "";
				}
				
				response.setLista(municipioService.listarMunicipio(pagenumber,pagesize,nombre,activo));
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje(e.getMessage());
			}
			
		return ResponseEntity.ok(response);
    }
	
	@PostMapping("/InsertarMunicipio")
    public ResponseEntity<ResponseInsertarMunicipio> insertarMunicipio
    (@RequestBody Municipio municipio)
	{  
		ResponseInsertarMunicipio response = new ResponseInsertarMunicipio();
			
			try {
				
				Integer temp = this.municipioService.verificarNombre(municipio.getMuni_Nombre(), 0);
				
				if(temp == 0) {
					Integer indice = this.municipioService.insertarMunicipio(municipio);
					response.setMensaje("Se han insertado los datos del Municipio");
					response.setMuni_Id(indice);
				}else {
					response.setCodError(1);
					response.setMensaje("El nombre ya está registrado");
					response.setEstado(false);
				}
				
					
			}catch(Exception e) {
				response.setCodError(0);
				response.setMensaje("Error no controlado");
				response.setEstado(false);
			}

		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/ActualizarMunicipio")
	public ResponseEntity<ResponseActualizarMunicipio> actualizarMunicipio
    (@RequestBody Municipio municipio)
	{ 
		ResponseActualizarMunicipio response = new ResponseActualizarMunicipio();
		response.setEstado(true);

		try {
			Integer temp1 = this.municipioService.verificarNombre(municipio.getMuni_Nombre(), municipio.getMuni_Id());

			if(temp1 == 0) {
				this.municipioService.actualizarMunicipio(municipio);
				response.setMensaje("Se han actualizado los datos del Municipio");
				response.setMuni_Id(municipio.getMuni_Id());
			}else {
				response.setCodError(1);
				response.setMensaje("El nombre ya está registrado");
				response.setEstado(false);
			}
			
		}catch(Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlador");
			response.setEstado(false);
			
		}
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/SeleccionarMunicipio/{idmunicipio}")
	public ResponseEntity<ResponseSeleccionarMunicipio> seleccionarMunicipio(@PathVariable("idmunicipio")Integer idmunicipio) {
		ResponseSeleccionarMunicipio response = new ResponseSeleccionarMunicipio();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.municipioService.validarMunicipioExiste(idmunicipio);
			
			if(temp == 1) {
				response.setMunicipio(this.municipioService.seleccionarMunicipio(idmunicipio));
			}else {
				response.setCodError(1);
				response.setMensaje("Municipio no registrado");
				response.setEstado(false);
			}			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/EliminarMunicipio/{idmunicipio}/{idtarifa}")
	public ResponseEntity<ResponseEliminarMunicipio> eliminarMunicipio(@PathVariable("idmunicipio")Integer idmunicipio,
			@PathVariable("idtarifa") Integer idtarifa) {
		ResponseEliminarMunicipio response = new ResponseEliminarMunicipio();
		response.setEstado(true);
		
		try {
			
			Integer temp = this.municipioService.validarMunicipioExiste(idmunicipio);
			
			if(temp == 1) {
				Integer temp2 = this.municipioService.validarMunicipioDependencias(idmunicipio);
				
				if(temp2==0) {
					this.municipioService.eliminarMunicipio(idmunicipio,idtarifa);
					response.setResultado(true);
				}else {
					response.setCodError(2);
					response.setMensaje("Municipio tiene dependencias en zonas");
					response.setEstado(false);
				
				}
				
			}else {
				response.setCodError(1);
				response.setMensaje("Municipio no registrado");
				response.setEstado(false);
			}
			
			
		}catch(Exception e ) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}
}
