package app.parqueo.infrastructure.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.ClienteEntity;

public interface ClienteRepository  extends JpaRepository<ClienteEntity, Integer>
{
	@Query(value = "SELECT * FROM \"UFN_ListarCliente\"(:pagenumber,:pagesize,:nombre, :nrodocumento, :placa)", nativeQuery = true)
	List<Object[]> listarCliente(
			@Param("pagenumber") Integer pagenumber, 
			@Param("pagesize") Integer pagesize,
			@Param("nombre") String nombre,
			@Param("nrodocumento") String nrodocumento,
			@Param("placa") String placa
			);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarClienteExiste\"(:idcliente)", nativeQuery = true)
	Integer validarClienteExiste(@Param("idcliente") Integer idcliente);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarDetalleCliente\"(:idcliente)", nativeQuery = true)
	List<Object[]> seleccionarDetalleCliente(@Param("idcliente") Integer idcliente);
}