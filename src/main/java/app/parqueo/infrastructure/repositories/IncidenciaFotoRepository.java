package app.parqueo.infrastructure.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.IncidenciaFotoEntity;

public interface IncidenciaFotoRepository extends JpaRepository<IncidenciaFotoEntity, Integer>{

	@Query(value = "SELECT * FROM \"UFN_ListarIncidenciaFoto\"(:idincidencia)", nativeQuery = true)
	List<IncidenciaFotoEntity> listarIncidenciaFoto(@Param("idincidencia") Integer idincidencia);
}
