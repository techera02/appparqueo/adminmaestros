package app.parqueo.infrastructure.repositories;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.TransaccionEntity;

public interface TransaccionRepository extends JpaRepository<TransaccionEntity, Integer>
{
	@Query(value = "CALL \"USP_RecargarBilleteraWeb_PROC\"(:clie_id,:monto,:frec_id,:tarj_id)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer recargarBilletera(@Param("clie_id")Integer clie_id,@Param("monto") BigDecimal monto, 
			@Param("frec_id")Integer frec_id,@Param("tarj_id") Integer tarj_id);
	
	@Query(value = "SELECT \"UNF_ValidarRecargaBilletera\"(:clie_id,:frec_id,:tarj_id)", nativeQuery = true)
	Integer ValidarRecargaBilletera(@Param("clie_id")Integer clie_id, @Param("frec_id")Integer frec_id,
			@Param("tarj_id") Integer tarj_id);
	


}
