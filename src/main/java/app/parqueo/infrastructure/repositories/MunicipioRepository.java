package app.parqueo.infrastructure.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.MunicipioEntity;


public interface MunicipioRepository  extends JpaRepository<MunicipioEntity, Integer>

{
	@Query(value = "SELECT * FROM \"UFN_ListarMunicipio\"(:pagenumber,:pagesize,:nombre,:activo)", nativeQuery = true)
	List<Object[]> listarMunicipio(@Param("pagenumber") Integer pagenumber, @Param("pagesize") Integer pagesize,
			@Param("nombre") String nombre, @Param("activo") Integer activo);
	
	@Query(value = "SELECT \"UFN_InsertarMunicipio\"(:muni_nombre,:muni_activo,:muni_usuariocreacion)", nativeQuery = true)
	Integer insertarMunicipio(
			@Param("muni_nombre") String muni_nombre, @Param("muni_activo") boolean muni_activo,
			@Param("muni_usuariocreacion") Integer muni_usuariocreacion
			);
	
	@Query(value = "CALL \"USP_Municipio_UPD\"(:muni_id,:muni_nombre,:muni_activo,:muni_usuarioedicion)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarMunicipio(
			@Param("muni_id") Integer muni_id, @Param("muni_nombre") String muni_nombre,
			@Param("muni_activo") boolean muni_activo, @Param("muni_usuarioedicion") Integer muni_usuarioedicion
			);
		
	@Query(value = "SELECT \"UFN_ValidarMunicipioExiste\"(:muni_nombre,:idmunicipio)", nativeQuery = true)
	Integer validarNombreMunicipio(@Param("muni_nombre") String muni_nombre, @Param("idmunicipio") Integer idmunicipio);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarMunicipioExistePorId\"(:idmunicipio)", nativeQuery = true)
	Integer validarMunicipioExiste(@Param("idmunicipio") Integer idmunicipio);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarMunicipio\"(:idmunicipio)", nativeQuery = true)
	MunicipioEntity seleccionarMunicipio(@Param("idmunicipio") Integer idmunicipio);
	
	@Query(value = "CALL \"USP_Municipio_DEL\"(:idmunicipio,:idtarifa)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer eliminarMunicipio(@Param("idmunicipio") Integer idmunicipio, @Param("idtarifa") Integer idtarifa);
	
	@Query(value = "SELECT \"UFN_ValidarMunicipioDenpendencias\"(:idmuni)", nativeQuery = true)
	Integer validarMunicipioDependencias(@Param("idmuni") Integer idmuni);
}