package app.parqueo.infrastructure.repositories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.IncidenciaEntity;

public interface IncidenciaRepository extends JpaRepository<IncidenciaEntity, Integer> {

	@Query(value = "SELECT * FROM \"UFN_ListarIncidencia\"(:pagenumber,:pagesize,:fechainicio,:indicefechainicio,"
			+ ":fechafin,:indicefechafin,:tipoincidencia,:placa)", 
			nativeQuery = true)
	List<Object[]> listarIncidencia(
			@Param("pagenumber") Integer pagenumber, 
			@Param("pagesize") Integer pagesize,
			@Param("fechainicio")Date fechainicio,
			@Param("indicefechainicio")Integer indiceFechainicio,
			@Param("fechafin")Date fechafin,
			@Param("indicefechafin")Integer indiceFechafin,
			@Param("tipoincidencia") Integer tipoincidencia,
			@Param("placa") String placa
			);

	@Query(value = "SELECT * FROM \"UFN_ValidarIncidenciaExiste\"(:idincidencia)", nativeQuery = true)
	Integer validarIncidenciaExiste(@Param("idincidencia") Integer idincidencia);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarIncidencia\"(:idincidencia)", nativeQuery = true)
	List<Object[]> seleccionarIncidencia(@Param("idincidencia") Integer idincidencia);
}
