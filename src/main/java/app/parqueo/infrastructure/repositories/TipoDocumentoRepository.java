package app.parqueo.infrastructure.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.parqueo.infrastructure.postgresql.entities.TipoDocumentoEntity;

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumentoEntity, Integer>{

	@Query(value = "SELECT * FROM \"UFN_ListarTipoDocumento\"()", nativeQuery = true)
	List<TipoDocumentoEntity> listarTipoDocumento();
}
