package app.parqueo.infrastructure.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.SupervisorEntity;

public interface SupervisorRepository  extends JpaRepository<SupervisorEntity, Integer>

{
	
	@Query(value = "SELECT \"UFN_InsertarSupervisor\"(:supe_nombres,:supe_apellidopaterno,:supe_apellidomaterno,:tdoc_id,:supe_nrodocumento,:supe_tokenfirebase,:supe_activo,:supe_usuariocreacion,:supe_clave,:supe_alias,:supe_correo)", nativeQuery = true)
	Integer insertarSupervisor(
			@Param("supe_nombres") String supe_nombres, @Param("supe_apellidopaterno") String supe_apellidopaterno,
			@Param("supe_apellidomaterno") String supe_apellidomaterno, @Param("tdoc_id") Integer tdoc_id,
			@Param("supe_nrodocumento") String supe_nrodocumento, @Param("supe_tokenfirebase") String supe_tokenfirebase,
			@Param("supe_activo") boolean supe_activo, @Param("supe_usuariocreacion") Integer supe_usuariocreacion,
			@Param("supe_clave") String supe_clave, @Param("supe_alias") String supe_alias,
			@Param("supe_correo") String supe_correo
			);
	
	@Query(value = "CALL \"USP_Supervisor_UPD\"(:supe_id,:supe_nombres,:supe_apellidopaterno,:supe_apellidomaterno,:tdoc_id,:supe_nrodocumento,:supe_tokenfirebase,:supe_activo,:supe_usuarioedicion,:supe_correo)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer actualizarSupervisor(
			@Param("supe_nombres") String supe_nombres, @Param("supe_apellidopaterno") String supe_apellidopaterno,
			@Param("supe_apellidomaterno") String supe_apellidomaterno, @Param("tdoc_id") Integer tdoc_id,
			@Param("supe_nrodocumento") String supe_nrodocumento, @Param("supe_tokenfirebase") String supe_tokenfirebase,
			@Param("supe_activo") boolean supe_activo, @Param("supe_usuarioedicion") Integer supe_usuarioedicion,
			@Param("supe_correo") String supe_correo, @Param("supe_id") Integer supe_id
			);
	
	@Query(value = "SELECT \"UFN_ValidarCorreoSupervisor\"(:correo,:idsupervisor)", nativeQuery = true)
	Integer validarCorreoSupervisor(@Param("correo") String correo, @Param("idsupervisor") Integer idsupervisor);

	@Query(value = "SELECT \"UFN_ValidarAliasSupervisorExiste\"(:supe_alias)", nativeQuery = true)
	Integer validarAliasSupervisor(@Param("supe_alias") String supe_alias);
	
	@Query(value = "SELECT * FROM \"UFN_ListarSupervisor\"(:pagenumber,:pagesize,:nombre,:documento,:activo)", nativeQuery = true)
	List<Object[]> listarSupervisor(@Param("pagenumber") Integer pagenumber, @Param("pagesize") Integer pagesize,
			@Param("nombre") String nombre, @Param("documento") String documento, @Param("activo") Integer activo);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarSupervisorExiste\"(:idsupervisor)", nativeQuery = true)
	Integer validarSupervisorExiste(@Param("idsupervisor") Integer idsupervisor);
	
	@Query(value = "CALL \"USP_Supervisor_DEL\"(:idsupervisor)", nativeQuery = true)
	@Modifying(clearAutomatically = true)
	@Transactional
	Integer eliminarSupervisor(@Param("idsupervisor") Integer idsupervisor);
	
	@Query(value = "SELECT \"UFN_ValidarDocumentoSupervisor\"(:documento,:idsupervisor)", nativeQuery = true)
	Integer validarDocumentoSupervisor(@Param("documento") String documento, @Param("idsupervisor") Integer idsupervisor);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarSupervisor\"(:idsupervisor)", nativeQuery = true)
	SupervisorEntity seleccionarSupervisor(@Param("idsupervisor") Integer idsupervisor);
	
	@Query(value = "SELECT \"UFN_RecuperarContraseniaSupervisor\"(:correo,:clave)", nativeQuery = true)
	Integer recuperarContraseniaSupervisor(@Param("correo") String correo, @Param("clave") String clave);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarSupervisorPorCorreo\"(:correo)", nativeQuery = true)
	SupervisorEntity seleccionarSupervisorPorCorreo(@Param("correo") String correo);
}
